# 코드 내보내기

## 시작하기 <a name="getting-started"></a>
테스트 또는 테스트 수트를 마우스 오른쪽 단추로 클릭하여 `Export`를 선택한 다음, 대상 언어를 선택하고 `Export`를 클릭하여 테스트 또는 테스트 수트를 WebDriver 코드로 내보낼 수 있다.

![right-click](../images/right-click.png)

![menu](../images/menu.png)

이렇게 하면 내보낸 대상 언어의 코드가 포함된 파일이 브라우저의 다운로드 디렉토리에 저장된다.

### 오리진 추적 코드 주석 <a name="origin-comments"></a>
내보낼 때 오리진 추적 코드 주석을 활성화하는 선택적 토글이 있다.

내보낸 파일에 인라인 코드 주석을 추가하고 해당 주석을 생성한 Selenium IDE의 테스트 단계에 대한 세부 정보를 표시한다.

## 지원되는 내보내기 <a name="supported-exports"></a>
현재 다음 언어와 테스트 프레임워크로 내보내기를 지원하고 있다.

- C# NUnit
- Java JUNit
- JavaScript Mocha
- Python pytest

Selenium에 대해 공식적으로 지원되는 모든 프로그래밍 언어 바인딩(예: Java, JavaScript, C#, Python 및 Ruby)을 각 언어에 대해 적어도 하나의 테스트 프레임워크에서 지원할 계획이 있다.

새로운 언어를 추가하고 특정 언어에 대한 프레임워크를 테스트하는 데 도움이 되는 기여를 환영한다. 자세한 내용은 [기여 방법](https://www.selenium.dev/selenium-ide/docs/en/introduction/code-export#how-to-contribute)을 참조하세오.

<span style="color:red">우선 Python과 pytest에 대하여만 편역하였다. 이후 필요에 따라 다음 언어도 추가하려고 한다.</span>

### Python pytest <a name="pytest"></a>
Python pytest용으로 내보낸 코드는 Python 3, pytest 4.6.x 및 최신 버전의 Selenium과 함께 작동하도록 빌드되었다.

내보낸 JavaScript 파일을 가져와 이러한 종속성(예: `pip3 install`)을 설치한 다음 실행할 수 있다.

다음은 시작하는 데 도움을 주는 `requirements.txt`의 예이다.

```
pytest == 4.6.3
selenium == 4.0.0a1
```

```shell
pip3 install -r ./requirements.txt
```

## 기여하는 방법 <a name="how-to-contribute"></a>

<span style="color:red">향후 기여할 수있을 때 편역할 것이다.</span>
