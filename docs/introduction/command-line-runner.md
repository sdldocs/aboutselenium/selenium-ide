# 명령어 실행기 (Command-line Runner)
이제 코드를 작성할 필요 없이 모든 Selenium IDE 테스트를 브라우저, 병렬 및 그리드에서 실행할 수 있습니다.

Selenium IDE 명령어 실행기를 설치하고, (로컬에서 테스트를 실행하는 경우) 필요한 브라우저 드라이버를 가져오고, 원하는 옵션을 사용하여 명령 프롬프트에서 실행기를 실행할 수 있다.

![runner](../images/runner.png)

## 전제조건 <a name="prerequisites"></a>
명령어 실행기가 작동하려면 다음과 같은 종속성이 필요합니다:

- `node`(Node.js 프로그래밍 언어) version 8 또는 10
- `npm`(NodeJS 패키지 관리자)일반적으로 `node`와 함께 설치
- `selenium-side-runner`(Selenium IDE 명령어 수행기)
- 및 사용할 브라우저 드라이버(다음 섹션에 자세한 내용)

```bash
> brew install node
> npm install -g selenium-side-runner
```

> **Note** : 시스템 구성은 위의 샘플에 사용된 것과 다를 수 있다 (예: MacOS의 Homebrew). 이 경우 패키지 관리자의 Node 설치 설명서를 참조하거나 Node 다운로드 페이지에서 운영 체제의 Node 설치 관리자를 직접 다운로드한다.

## 브라우저 드라이버 설치 <a name="installing-browser-driver"></a>
로컬에서 테스트를 실행하려면 각 브라우저에 대해 몇 가지 추가 설정이 필요하다.

Selenium은 브라우저 드라이버라고 불리는 작은 바이너리 애플리케이션을 통해 각 브라우저와 통신한다. 각 브라우저에는 시스템 경로를 수동으로 다운로드하여 추가하거나 패키지 관리자를 사용하여 최신 버전의 브라우저 드라이버를 설치할 수 있는 고유한 브라우저가 있다(권장).

또한 컴퓨터에 브라우저가 설치되어 있어야 한다.

### Chrome <a name="chrome"></a>
Chrome의 경우, ChromeDriver가 필요하다.

```bash
> npm install -g chromedriver
```

### Edge <a name="edge"></a>
Microsoft Edge의 경우, Windows에서 실행되어야 하며 EdgeDriver도 필요하다.

```bash
> npm install -g edgedriver
```

### Firefox <a name="firefox"></a>
Firefox의 경우, geckodriver가 필요하다.

```bash
> npm install -g geckodriver
```

### Internet Explorer <a name="ie"></a>
Internet Explorer의 경우, 윈도우에서 실행되어야 하며 IEDriver도 필요하다.

```bash
> npm install -g iedriver
```

IEDriver가 동작하려면 몇 가지 추가 설정이 필요하다. 자세한 내용은 여기를 참조한다.

### Safari <a name="safari"></a>
Safari의 경우, SafariDriver가 필요하다.

최신 버전의 Safari와 함께 제공된다. 컴퓨터에서 이 기능을 사용하려면 몇 가지 단계를 수행해야 한다. 자세한 내용은 SafariDriver 설명서의 이 섹션을 참조한다.

## 수행기 실행 <a name="launching-runner"></a>
모든 것이 설치되면 명령으로 `selenium-side-runner`를 이전에 저장한 프로젝트 파일의 경로함께 호출하면 테스트를 실행할 수 있다 (시작하기 참조).

```bash
> selenium-side-runner /path/to/your-project.side
```

> **Note**: `.side` 파일이 여럿 있는 경우 와일드카드(예: `/path/to/*.side`)를 사용할 수 있다.

이 명령을 실행하면 여러 브라우저 창에서 `n`개의 프로세스(여기서 `n`은 시스템에서 사용 가능한 CPU 코어의 수)에 분산된 테스트가 병렬로 시작된다.

프로세스 수는 제공할 수 있는 다양한 인수를 통해 런타임에 구성할 수 있다.

> **Note**: 병렬 실행은 수트 수준에서 자동으로 수행된다. 수트 내에서 테스트를 병렬로 실행하려면 설정을 변경해야 한다. 자세한 내용은 수트의 병렬화 테스트를 한다.

## 런타임 구성<a name="run-time-configuration"></a>
실행기를 사용하면 런타임에 다른 구성 인수를 전달할 수 있다.

### 다른 브라우저에서 로컬로 실행 <a name="running-different-browser-locally"></a>
기능의 가장 일반적인 사용은 로컬에서 테스트 실행을 위해 다른 브라우저를 지정하는 것이다.

```shell
selenium-side-runner -c "browserName=chrome"
selenium-side-runner -c "browserName='internet explorer'"
selenium-side-runner -c "browserName=edge"
selenium-side-runner -c "browserName=firefox"
selenium-side-runner -c "browserName=safari"
```

> **Note**: 로컬에서 테스트를 실행할 때 각 브라우저에 대해 일부 설정이 필요하다. 자세한 내용은 브라우저 드라이버 설치를 참조한다.

### Selenium Grid에서 실행 <a name="running-selenium-grid"></a>
Grid(예: 자체 Grid 또는 Source Labs와 같은 호스팅 공급자)에서 테스트를 실행하려면 다양한 기능과 함께 테스트를 지정할 수 있다.

```shell
selenium-side-runner --server http://localhost:4444/wd/hub -c "browserName='internet explorer' version='11.0' platform='Windows 8.1'"
```

`-server`는 Grid에 URL을 지정하고, `-c`는 Grid에서 사용할 자원을 지정합니다.

여기(?)에서 사용 가능한 자원의 전체 목록을 볼 수 있다.

### 병렬 프로세스 수 지정 <a name="specify-number-parallel-processes"></a>
Grid를 실행할 때는 실행 중인 병렬 세션 수를 제어할 수 있다. 이를 위해 `-w n` 명령 플래그를 사용할 수 있다 (여기서 `n`은 원하는 프로세스 수이다).

```shell
selenium-side-runner -w 10 --server http://localhost:4444/wd/hub
```

실행기는 작업자 수를 컴퓨터에서 사용할 수 있는 CPU 코어 수로 자동 설정한다. 대부분의 경우 이 옵션이 가장 좋다.

### Chrome 관련 기능 <a name="chrome-specific-capabilities"></a>
표준 위치가 아닌 곳의 콤퓨터에 Chrome을 설치한 경우, ChromeDriver가 어디를 찾아야 하는지 알 수 있도록 경로를 지정할 수 있다.

```shell
selenium-side-runner -c "goog:chromeOptions.binary='/path/to/non-standard/Chrome/install'"
```

Chrome 관련 기능을 사용하면 테스트를 머리없이 실행할 수도 있다.

```shell
selenium-side-runner -c "goog:chromeOptions.args=[disable-infobars, headless]"
```

## 손안의 프레임워크 <a name="framework-fingertips"></a>
실행기와 함께 상자에서 나오는 다른 친절함들도 있다. 기존의 테스트 자동화 프레임워크에서 사용할 수 있을 것으로 예상되는 것들.

### 베이스 URL 변경 <a name="change-base-url"></a>
다른 기본 URL을 지정할 수 있는 기능을 통해 다양한 환경(예: 로컬 개발, 테스트, 스테이징, 프로덕션)에서 테스트를 쉽게 지정할 수 있다.

```shell
selenium-side-runner --base-url https://localhost
```

### 필터 테스트 <a name="filter-test"></a>
`--filter target` 명령 플래그(`target`은 정규식 값)를 사용하여 테스트 대상의 부분 집합을 실행할 수도 있다. 주어진 검색 기준을 포함하는 테스트 이름만 실행된다.

```shell
selenium-side-runner --filter smoke
```

### 테스트 결과를 파일로 출력 <a name="output-test"></a>
CI 프로세스의 일부로 실행되는 경우와 같이 테스트 결과를 파일로 내보내려면, `--output-directory`와 `--output-format` 플래그를 조합하여 사용할 수 있다.

`--output-directory`는 테스트 결과 파일을 저장할 위치를 정의한다. 절대 경로 또는 상대 경로를 사용할 수 있다.

`--output-format`은 테스트 결과 파일 형식을 정의한다. `jest`(예: JSON) 또는 `junit`(예: XML)일 수 있다. 기본 형식은 `jest`이다(예: 형식을 지정하지 않은 경우).

```shell
selenium-side-runner --output-directory=results
# Outputs results in `jest` frormat in `./results/projectName.json'
```

```shell
selenium-side-runner --output-directory=results --output-format=jest
# Outputs results in `jest` frormat in `./results/projectName.json'
```

```shell
selenium-side-runner --output-directory=results --output-format=junit
# Outputs results in `junit` frormat in `./results/projectName.xml'
```

### 기본 구성 지정 <a name="specify-default-configuration"></a>
명령어의 필요한 인수를 모두 기억하는 대신 런타임 매개 변수를 구성 파일에 저장할 수 있다.

사용할 수 있는 두 가지 설정 파일이 있다.

#### 옵션 1
테스트를 실행할 디렉토리에 `.side.yml` 파일을 만든다. 실행기는 자동으로 파일을 찾는다. 파일 내용의 예는 다음과 같다.

```yml
capabilities:
  browserName: "firefox"
baseUrl: "https://www.seleniumhq.org"
server: "http://localhost:4444/wd/hub"
```

파일을 무시하고 명령어 인수를 대신 사용하려면 실행 시 명령과 함께 `--no-sideyml`을 사용할 수 있다.

#### 옵션 2
`.side.yml` 파일을 사용하는 대신, 선택한 YAML 파일에서 런타임 매개 변수를 지정할 수 있으며, 다음 테스트를 실행할 때 해당 위치를 지정할 수 있다.

```shell
selenium-side-runner --config-file "/path/to/your/config.yaml"
```

> **Note**: `--config-file` 플래그를 사용하는 경우 `.side.yml`은 무시된다.

## Selenium IDE 구성 <a name="selenium-ide-configuration"></a>

### 제품군에서 병렬화 테스트 <a name="test-parallelization-in-suite"></a>
실행기는 즉시 수트를 병렬로 실행하지만, 수트 내의 테스트는 순차적으로 실행된다.

주어진 수트에서 테스트를 병렬로 실행하려면 Selenium IDE에서 해당 수트에 대한 설정을 변경해야 한다.

1. Selenium IDE에서 `Test Suite` 뷰로 전환
2. 구성할 수트 이름 옆에 있는 드롭다운 메뉴를 클릭하고 설정을 클릭한다
3. `Run in parallel`을 위하여 checkbox를 클릭한다
4. `submit`를 클릭한다
5. Selenium IDE 프로젝트 파일을 저장

둘 이상의 수트가 이 방법으로 실행되도록 구성하려면 각 수트에 대하여 1-4단계를 반복한다. 완료하면 프로젝트 파일을 저장한다.

## 고급 옵션 <a name="advanced-options"></a>

### 추가 매개변수 <a name="additional-params"></a>
Selenium IDE용 플러그인은 고유한 런타임 매개 변수를 지정할 수 있다. `--params` 플래그를 통해 사용할 수 있다.

이 옵션은 기능을 지정하는 방법과 유사하게 다양한 옵션 문자열을 사용한다.

#### 기본 사용법
매개 변수의 이름과 값을 지정한다. 가장 기본적인 방법은 문자열로 지정하는 것이다.

```shell
selenium-side-runner --params "a='example-value'"
```

#### 중첩 사용
dot-notation을 사용하여 매개변수를 중첩할 수도 있다.

```shell
selenium-side-runner --params "a.b='another example-value'"
```

#### 배열 값
문자열 대신 영숫자 값의 배열을 지정할 수 있다.

```shell
selenium-side-runner --params "a.b.c=[1,2,3]"
```

#### 다중 매개변수
`--params`는 한 번만 호출할 수 있지만 공백으로 구분하여 여러 매개 변수를 지정할 수 있다.

```shell
selenium-side-runner --params "a='example-value' a.b='another example-value' a.b.c=[1,2,3]"
```

### 프록시 서버 사용 <a name="using-proxy-server"></a>
실행기에서 다음 옵션을 사용하여 브라우저에 프록시 기능을 전달할 수 있다.

#### 직접 프록시
이 옵션은 모든 브라우저 프록시를 바이패스하도록 WebDriver를 구성한다.

명령으로

```shell
selenium-side-runner --proxy-type=direct
```

`.side.yml`에서

```yml
proxyType: direct
```

#### 수동 프록시
브라우저 프록시를 수동으로 설정한다.

명령으로

```shell
selenium-side-runner --proxy-type=manual --proxy-options="http=localhost:434 bypass=[http://localhost:434, http://localhost:8080]"
```

`.side.yml`에서

```yml
proxyType: manual
proxyOptions:
  http: http://localhost:434
  https: http://localhost:434
  ftp: http://localhost:434
  bypass:
    - http://localhost:8080
    - http://host:434
    - http://somethingelse:32
```

#### PAC 프록시
지정된 URL의 PAC 파일을 사용하여 브라우저 프록시를 설정하도록 WebDriver를 구성한다.

명령으로

```shell
selenium-side-runner --proxy-type=pac --proxy-options="http://localhost/pac"
```

`.side.yml`에서

```yml
proxyType: pac
proxyOptions: http://localhost/pac
```

#### SOCKS 프록시
SOCKS 프록시에 대한 프록시 설정을 생성한다.

명령으로

```shell
selenium-side-runner --proxy-type=socks --proxy-options="socksProxy=localhost:434 socksVersion=5"
```

`.side.yml`에서

```yml
proxyType: socks
proxyOptions:
  socksProxy: localhost:434
  socksVersion: 5
```

#### 시스템 프록시
현재 시스템의 프록시를 사용하도록 WebDrive를 설정한다.

명령으로

```shell
selenium-side-runner --proxy-type=system
```

`.side.yml`에서

```yml
proxyType: system
```

### 코드 내보내기 <a name="code-export"></a>
레코딩된 테스트를 WebDriver 코드로 변환하는 방법을 배우거나 레코딩된 테스트를 기존 커스텀 테스트 프레임워크에 통합하려는 경우, 이제 선택한 언어에서 사용할 수 있는 코드 내보내기가 필요하다. [여기](code-export.md)를 참고한다.
